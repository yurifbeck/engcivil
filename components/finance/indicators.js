
function ma (data, size) {
  const length = data.length
  // console.log('SMA ', 'data =', data[0], ' size = ', size, 'length = ', length)
  function isNumber (subject) {
    if (typeof subject === 'number') {
      return true
    }
  }

  if (!size) {
    return data.reduce((a, b) => a + b) / length
  }

  if (size <= 1) {
    return data.slice()
  }

  if (size > length) {
    return Array(length)
  }

  const prepare = size - 1
  const ret = []
  let sum = 0
  let i = 0
  let counter = 0
  let datum

  for (; i < length && counter < prepare; i++) {
    datum = data[i]
    // console.log('datum ', datum, 'i', i, 'sum', sum, 'counter ', counter, ' size ', size)

    if (isNumber(datum)) {
      sum += datum
      counter++
    }
  }

  for (; i < length; i++) {
    datum = data[i]
    // console.log('datum ', datum, 'i', i, 'sum', sum, 'counter ', counter, ' size ', size)

    if (isNumber(datum)) {
      sum += datum
    }

    if (isNumber(data[i - size])) {
      sum -= data[i - size]
    }

    ret[i] = +(sum / size).toFixed(2)
  }
  // console.log('ma calculated')
  // for (let index = 0; index < ret.length; index++) {
  //   if (ret[index] === undefined) {
  //     ret[index] = 0
  //   }
  // }
  // var undef = ret.pop(size)
  ret.splice(0, size)
  // console.log('ret', ret)
  // console.log('filted', filtered)
  return ret.concat(Array(size))
}

function maAnalysis (sma50, sma200) {
  // console.log('maAnalysis')
  // console.log('sma50', sma50, 'sma200', sma200)
  // sma200 array is smaller, so make them equal length
  let sma50sliced = sma50.slice(0, -200)
  // let sma200sliced = sma200.slice(0, -200)
  // console.log('checkLength', sma50sliced.length, sma200sliced.length)

  // get max diff from each to know how far historically they get
  let maxDiff = 0
  let diff = []
  for (let i = 0; i < sma50sliced.length; i++) {
    let dayDiff = +Math.abs(sma50sliced[i] - sma200[i]).toFixed(2)
    diff.push(dayDiff)
    if (dayDiff > maxDiff) maxDiff = dayDiff
  }

  // get last day sma and compare to max diff
  let todaySMA200 = +sma200[0]
  let todaySMA50 = +sma50[0]
  let todayDiff = +(todaySMA200 - todaySMA50).toFixed(2)
  let analysis = +(todayDiff / maxDiff).toFixed(2)

  let maAnalysis = {
    'diff': diff,
    'maxDiff': maxDiff,
    'todayDiff': todayDiff,
    'analysis': analysis,
    'sma200': todaySMA200,
    'sma50': todaySMA50
  }
  console.log(maAnalysis)

  return maAnalysis
}

function rstd (data, size) {
  const length = data.length
  function isNumber (subject) {
    if (typeof subject === 'number') {
      return true
    }
  }

  if (!size) {
    return data.reduce((a, b) => a + b) / length
  }

  if (size <= 1) {
    return data.slice()
  }

  if (size > length) {
    return Array(length)
  }

  const prepare = size - 1
  const ma = []
  const ret = []
  let sum = 0
  let i = 0
  let counter = 0
  let datum

  for (; i < length && counter < prepare; i++) {
    datum = data[i]

    if (isNumber(datum)) {
      sum += datum
      counter++
    }
  }

  for (; i < length; i++) {
    datum = data[i]

    if (isNumber(datum)) {
      sum += datum
    }

    if (isNumber(data[i - size])) {
      sum -= data[i - size]
    }

    ma[i] = +(sum / size).toFixed(2)
  }
  // console.log('ma calculated')
  // for (let index = 0; index < ret.length; index++) {
  //   if (ret[index] === undefined) {
  //     ret[index] = 0
  //   }
  // }
  // var undef = ret.pop(size)
  ret.splice(0, size)
  // console.log('ret', ret)
  // console.log('filted', filtered)
  return ret.concat(Array(size))
}

function mstd (array, window) {
  let mstd = []
  for (let i = 0; i < array.length - window; i++) {
    const windowSTD = standardDeviation(array.slice(i, i + window)).toFixed(2)
    mstd.push(windowSTD)
  }

  return mstd
}

function standardDeviation (values) {
  var avg = average(values)

  var squareDiffs = values.map(function (value) {
    var diff = value - avg
    var sqrDiff = diff * diff
    return sqrDiff
  })

  var avgSquareDiff = average(squareDiffs)

  var stdDev = Math.sqrt(avgSquareDiff)
  return stdDev
}

function average (data) {
  var sum = data.reduce(function (sum, value) {
    return sum + value
  }, 0)

  var avg = sum / data.length
  return avg
}

export { rstd, ma, maAnalysis, mstd }
