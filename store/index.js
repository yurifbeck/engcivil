import axios from 'axios'

export const state = () => ({
  ticker: {
    SMAS: []
  },
  tickerList: [],
  loading: true,
  websiteName: 'eng',
  websiteDescription: '+ prog',
  dateTo: new Date().toISOString().substr(0, 10),
  dateFrom: new Date(Date.now() - 86400000 * 365).toISOString().substr(0, 10),
  traces: [],
  bottomNav: 0,
  remove: [],
  accentColor: '#000000FF',
  palette: ['#397D8A', '#4AA2A4', '#2F4171', '#3B2441']
})

export const mutations = {
  CLEAR: (state) => {
    state.ticker.SMAS = []
    state.traces = []
  },
  SET_BOTTOM_NAV: (state, n) => {
    state.bottomNav = n
    if (n === 0) {
      state.accentColor = '#000000'
    }
  },
  SET_TICKER_LIST: (state, list) => {
    state.tickerList = list.data
  },
  SET_ACCENT_COLOR: (state, color) => {
    state.accentColor = color
  },

  SET_TICKER: (state, data) => {
    state.ticker = {

      ...state.ticker,
      name: data.name,
      history: data.history,
      ohlc: {
        date: Object.keys(data.history),
        open: Object.values(data.history).map(v => +v.open),
        close: Object.values(data.history).map(v => +v.close),
        high: Object.values(data.history).map(v => +v.high),
        low: Object.values(data.history).map(v => +v.low),
        volume: Object.values(data.history).map(v => +v.volume)
      }
    }
  },
  ADD_SMA: (state, obj) => {
    state.ticker.SMAS.push(obj)
  },
  UPDATE_SMA: (state, obj) => {
    state.ticker.SMAS.filter(v => { return v.id === obj.id })
  },
  REMOVE_SMA: (state, obj) => {
    let window = obj.window
    let data = obj.data
    let array = state.ticker.SMAS
    for (var i = array.length - 1; i >= 0; --i) {
      if (array[i].window === window && array[i].data === data) {
        array.splice(i, 1)
      }
    }
  },
  ADD_SMA_PLOT: (state, id) => {
    var index = state.ticker.SMAS.findIndex(v => { return v.id === id })
    state.ticker.SMAS[index].plot = true
  },
  REMOVE_SMA_PLOT: (state, id) => {
    var index = state.ticker.SMAS.findIndex(v => { return v.id === id })
    state.ticker.SMAS[index].plot = false
  },
  ADD_SMA_TRACE: (state, trace) => {
    state.traces.push(trace)
  },
  REMOVE_SMA_TRACE: (state, obj) => {
    let window = obj.window
    let data = obj.data
    let array = state.ticker.traces.SMAS
    for (var i = array.length - 1; i >= 0; --i) {
      if (array[i].window === window && array[i].data === data) {
        array.splice(i, 1)
      }
    }
  },
  START_LOADING: (state) => {
    state.loading = true
  },
  FINISH_LOADING: (state) => {
    state.loading = false
  },
  SET_DATEFROM: (state, date) => {
    state.dateFrom = date
  },
  SET_DATETO: (state, date) => {
    state.dateTo = date
  }
}

export const actions = {
  FETCH_ACAO ({ commit, state }, { symbol, dateFrom = '2015-01-01', sort = 'newest' }) {
    const token = 'XcJ3mcEl8YCuhShPvTWfkVK1xOF0lTRLEkoALPeW7h0CUrRNWtdtqwRBU8I1'
    return (

      fetch(`https://api.worldtradingdata.com/api/v1/history?symbol=${symbol}&date_from=${dateFrom}&sort=${sort}&api_token=${token}`).then(data => data.json()).then((res) => { commit('SET_ACAO', res) })
    )
  },
  async requestTickerData ({ commit, state }, params) {
    // API KEY
    const token = 'XcJ3mcEl8YCuhShPvTWfkVK1xOF0lTRLEkoALPeW7h0CUrRNWtdtqwRBU8I1'
    const symbol = params.symbol
    const dateFrom = this.state.dateFrom
    const dateTo = this.state.dateTo
    const sort = 'newest'
    // console.log('request ticker data: symbol= ', symbol, 'dateFrom = ', dateFrom)
    commit('START_LOADING')
    const { data } = await axios.get(`https://api.worldtradingdata.com/api/v1/history?symbol=${symbol}&date_from=${dateFrom}&date_to=${dateTo}&sort=${sort}&api_token=${token}`)
    // console.log('request ticker data', data)
    await commit('SET_TICKER', data)
    commit('FINISH_LOADING')
  },
  async searchTicker ({ commit, state }, symbol) {
    // API KEY
    const token = 'XcJ3mcEl8YCuhShPvTWfkVK1xOF0lTRLEkoALPeW7h0CUrRNWtdtqwRBU8I1'
    // console.log('request ticker data: symbol= ', symbol, 'dateFrom = ', dateFrom)
    const { data } = await axios.get(`https://api.worldtradingdata.com/api/v1/stock_search?search_term=${symbol}&search_by=symbol,name&limit=5&page=1&api_token=${token}`)
    // console.log('request ticker data', data)
    commit('SET_TICKER_LIST', data)
  },
  bottomNavChange ({ commit, state }, n) {
    let palette = ['#397D8A', '#4AA2A4', '#2F4171', '#3B2441']
    let randomColor = palette[Math.floor(Math.random() * palette.length)]
    commit('SET_ACCENT_COLOR', randomColor)
    commit('SET_BOTTOM_NAV', n)
  }
}
