import firebase from 'firebase/app'
import 'firebase/firestore'
if (!firebase.apps.length) {
  const config = {
    apiKey: 'AIzaSyCbJ49XyQgyeROswbuGc8unDlET6dpo5N0',
    authDomain: 'engcivil-33f7f.firebaseapp.com',
    databaseURL: 'https://engcivil-33f7f.firebaseio.com',
    projectId: 'engcivil-33f7f',
    storageBucket: 'engcivil-33f7f.appspot.com',
    messagingSenderId: '774603256708',
    appId: '1:774603256708:web:7770e5679516919d'
  }
  firebase.initializeApp(config)
}
const fireDb = firebase.firestore()
const timestamp = firebase.firestore.FieldValue.serverTimestamp()

export { fireDb, timestamp }
