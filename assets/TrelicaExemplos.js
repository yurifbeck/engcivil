class No {
  constructor (id, x, y, fx, fy, rx, ry) {
    this.id = id
    this.id1 = id + 1
    this.x = x
    this.y = y
    this.fx = fx
    this.fy = fy
    this.rx = rx
    this.ry = ry
    this.dof = {
      x: {
        globalId: this.id * 3,
        fGlobal: this.fx,
        fLocal: 0,
        r: this.rx
      },
      y: {
        globalId: this.id * 3 + 1,
        fGlobal: this.fy,
        fLocal: 0,
        r: this.ry
      },
      z: {
        globalId: this.id * 3 + 2,
        fGlobal: this.fz,
        fLocal: 0,
        r: this.rz
      }
    }
  }
}

class Elemento {
  constructor (id, nodeA, nodeB, inercia, elasticidade, area) {
    this.id = id
    this.id1 = id + 1
    this.nodeA = nodeA
    this.nodeB = nodeB
    this.inercia = inercia
    this.elasticidade = elasticidade
    this.area = area
  }

  get length () {
    var deltaX = this.nodeB.x - this.nodeA.x
    var deltaY = this.nodeB.y - this.nodeA.y
    return Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2)).toFixed(4)
  }

  get angle () {
    var deltaX = this.nodeB.x - this.nodeA.x
    var deltaY = this.nodeB.y - this.nodeA.y
    return (Math.atan2(deltaY, deltaX) * 180 / Math.PI).toFixed(4)
  }

  get k () {
    let I = this.inercia
    let E = this.elasticidade
    let L = this.length
    let k = [
      [
        12 * E * I / L ** 3,
        6 * E * I / L ** 2,
        -(12 * E * I / L ** 3),
        6 * E * I / L ** 2
      ],
      [
        6 * E * I / L ** 2,
        4 * E * I / L,
        -(6 * E * I / L ** 2),
        2 * E * I / L
      ],
      [
        -(12 * E * I / L ** 3),
        -(6 * E * I / L ** 2),
        12 * E * I / L ** 3,
        -(6 * E * I / L ** 2)
      ],
      [
        6 * E * I / L ** 2,
        2 * E * I / L,
        -(6 * E * I / L ** 2),
        4 * E * I / L
      ]
    ]
    return k
  }
}

class Estrutura {
  constructor (name, elementsArr, nodesArr) {
    this.name = name
    this.elementsArr = elementsArr
    this.nodesArr = nodesArr
  }
  get nNodes () {
    return this.nodesArr.length
  }
  get nElements () {
    return this.elementsArr.length
  }
}

// export default { No, Elemento, Estrutura }
var aNode0 = new No(0, 0, 0, 0, 0, true, false)
var aNode1 = new No(1, 8, 0, 0, -36000, false, false)
var aNode2 = new No(2, 14, 0, 0, 0, false, true)
var anNdesArr = [aNode0, aNode1, aNode2]
var aElement0 = new Elemento(0, aNode0, aNode1, 1, 1, 1)
var aElement1 = new Elemento(1, aNode1, aNode2, 1, 1, 1)
var aElementsArr = [aElement0, aElement1]
var viga = new Estrutura('viga 1', aElementsArr, anNdesArr)

var node0 = new No(0, 4, 4, 10, -15, false, false)
var node1 = new No(1, 4, 0, 0, 0, false, true)
var node2 = new No(2, 0, 0, 0, 0, true, true)
var nodesArr = [node0, node1, node2]
var element0 = new Elemento(0, node0, node1, 1, 1, 1)
var element1 = new Elemento(1, node1, node2, 1, 1, 1)
var element2 = new Elemento(2, node0, node2, 1, 1, 1)
var elementsArr = [element0, element1, element2]
var trelica1 = new Estrutura('trelica 1', elementsArr, nodesArr)
// console.log(trelica1)

var node0_ = new No(0, 0, 2, 0, 0, false, false)
var node1_ = new No(1, 2, 2, 0, 0, true, true)
var node2_ = new No(2, 4, 2, 0, 0, false, false)
var node3_ = new No(3, 6, 2, 0, 0, false, false)
var node4_ = new No(4, 8, 2, 0, 0, false, false)
var node5_ = new No(5, 6, 1, 0, 0, false, false)
var node6_ = new No(6, 4, 0, 0, 0, false, false)
var node7_ = new No(7, 2, 0, 0, 0, false, false)
var node8_ = new No(8, 0, 0, 0, 0, true, true)
var nodesArr_ = [node0_, node1_, node2_, node3_, node4_, node5_, node6_, node7_, node8_]

var element0_ = new Elemento(0, node0_, node1_, 1, 1, 1)
var element1_ = new Elemento(1, node1_, node2_, 1, 1, 1)
var element2_ = new Elemento(2, node2_, node3_, 1, 1, 1)
var element3_ = new Elemento(3, node3_, node4_, 1, 1, 1)
var element4_ = new Elemento(4, node4_, node5_, 1, 1, 1)
var element5_ = new Elemento(5, node5_, node6_, 1, 1, 1)
var element6_ = new Elemento(6, node6_, node7_, 1, 1, 1)
var element7_ = new Elemento(7, node7_, node8_, 1, 1, 1)
var element8_ = new Elemento(8, node0_, node7_, 1, 1, 1)
var element9_ = new Elemento(9, node1_, node7_, 1, 1, 1)
var element10_ = new Elemento(10, node2_, node7_, 1, 1, 1)
var element11_ = new Elemento(11, node2_, node6_, 1, 1, 1)
var element12_ = new Elemento(12, node2_, node5_, 1, 1, 1)
var element13_ = new Elemento(13, node3_, node5_, 1, 1, 1)
var element14_ = new Elemento(14, node8_, node0_, 1, 1, 1)
var elementsArr_ = [element0_, element1_, element2_, element3_, element4_, element5_, element6_, element7_, element8_, element9_, element10_, element11_, element12_, element13_, element14_]
var trelica2 = new Estrutura('trelica 2', elementsArr_, nodesArr_)

var node0A = new No(0, 0, 4, 0, 0, true, true)
var node1A = new No(1, 3, 4, 10, 0, false, false)
var node2A = new No(2, 0, 0, 0, 0, true, false)
var node3A = new No(3, 3, 0, 0, -10, false, false)
var node4A = new No(4, 6, 0, 0, 0, true, true)
var nodesArrA = [
  node0A,
  node1A,
  node2A,
  node3A,
  node4A
]
var element0A = new Elemento(0, node0A, node1A, 1, 1, 2)
var element1A = new Elemento(1, node0A, node2A, 1, 1, 2)
var element2A = new Elemento(2, node2A, node1A, 1, 1, 2)
var element3A = new Elemento(3, node1A, node3A, 1, 1, 2)
var element4A = new Elemento(4, node1A, node4A, 1, 1, 2)
var element5A = new Elemento(5, node2A, node3A, 1, 1, 2)
var element6A = new Elemento(6, node3A, node4A, 1, 1, 2)
var elementsArrA = [
  element0A,
  element1A,
  element2A,
  element3A,
  element4A,
  element5A,
  element6A
]
var trelica3 = new Estrutura('trelica 3', elementsArrA, nodesArrA)

var node0B = new No(0, 4, 0, 0, 0, true, true)
var node1B = new No(1, 4, 3, 10, 0, false, false)
var node2B = new No(2, 0, 0, 0, 0, true, true)
var node3B = new No(3, 0, 3, 0, 0, true, true)
var nodesArrB = [
  node0B,
  node1B,
  node2B,
  node3B
]
var element0B = new Elemento(0, node0B, node1B, 1, 1, 8)
var element1B = new Elemento(1, node1B, node2B, 1, 1, 8)
var element2B = new Elemento(2, node1B, node3B, 1, 1, 8)
var elementsArrB = [
  element0B,
  element1B,
  element2B
]
var trelica4 = new Estrutura('trelica 4', elementsArrB, nodesArrB)
var items = []
var vazio = {
  elementsArr: [],
  nodesArr: [],
  name: 'Novo'
}
items.push(vazio, viga, trelica1, trelica2, trelica3, trelica4)
export { items }
